
const sizeMap = function () {
  document.getElementById('map-container').style.position = 'absolute';
  document.getElementById('map-container').style.height = document.documentElement.clientHeight - 65 - 65 + 'px';
  document.getElementById('map-container').style.width = document.body.offsetWidth - 450 - 65 -65 + 'px';
  document.getElementById('map-container').style.top = '65px';
  document.getElementById('map-container').style.right = '65px';
};

sizeMap();

const map = L.map('map', {attributionControl: false, maxZoom: 16})

map.fitBounds([[6.80, -3.26],[6.25,-2.67]]);

window.onresize = function () {
  sizeMap();
  map.invalidateSize();
};

L.control.attribution({prefix: ''}).addTo(map);
L.control.scale({ imperial: false, position: 'bottomleft' }).addTo(map);

const mapClassesLandcover = '<div class=title>Landcover</div>' +
'<i class="cocoa"></i>Cocoa<br>' +
'<i class="agroforestry"></i>Agroforestry cocoa<br>' +
'<i class="forest-open"></i>Open canopy forest<br>' +
'<i class="forest-closed"></i>Closed canopy forest<br>' +
'<i class="oilpalm"></i>Oil Palm<br>' +
'<i class="agriculture"></i>Agriculture<br>' +
'<i class="urban"></i>Urban';

const legend = L.control({position: 'topright'});

legend.onAdd = function (map) {
  var div = L.DomUtil.create('div', 'info legend');
  div.innerHTML = mapClassesLandcover
  return div;
};

const layers = {
  background: {
    satellite: L.tileLayer('https://api.mapbox.com/styles/v1/ernstkui/{id}/tiles/256/{z}/{x}/{y}?access_token={accessToken}', {
      attribution: 'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'cjwbuvhct01ec1cmgz2k199s4',
      accessToken: 'pk.eyJ1IjoiZXJuc3RrdWkiLCJhIjoiY2owZHZjbGpvMDAxOTMzbzQxZ2Rva3NjaiJ9.tUgue-ESEzmwJYYosBRapQ',
      zIndex: 0
    }),
    contrast: L.tileLayer('https://api.mapbox.com/styles/v1/ernstkui/{id}/tiles/256/{z}/{x}/{y}?access_token={accessToken}', {
      attribution: 'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'cjx64a4fe17n21cnu89yuhwe2',
      accessToken: 'pk.eyJ1IjoiZXJuc3RrdWkiLCJhIjoiY2owZHZjbGpvMDAxOTMzbzQxZ2Rva3NjaiJ9.tUgue-ESEzmwJYYosBRapQ',
      zIndex: 0
    })
  },
  foreground: {
    landcover: L.tileLayer('landcover/{z}/{x}/{y}.png', {
      tms: true,
      opacity: 0.7,
      zIndex: 1,
			attribution: 'Satelligence cocoa landscape classification 2018'
    }),
    illegals: L.geoJSON(null, {
      style: function (feature) {
    		var color = '#f05a23';
    		var opacity = 0.1;
        return {'color':  color, 'weight': 1,  'dashArray': 4, 'fillOpacity': opacity};
      },
			attribution: 'Profiled illegal farms'
    }),
    admitted: L.geoJSON(null, {
      style: function (feature) {
    		var color = 'rgb(00, 60, 90)';
    		var opacity = 0.1;
        return {'color':  color, 'weight': 2, dashArray: 4, 'fillOpacity': opacity};
      },
			attribution: 'Admitted farms'
    }),
    parcs: L.geoJSON(null, {
      style: function (feature) {
    		var color = 'rgb(105, 140, 60)';
    		var opacity = 0.1;
        return {'color':  color, 'weight': 2, dashArray: 4, 'fillOpacity': opacity};
      },
			attribution: 'National Parc boundaries'
    }),
    satelligence: L.geoJSON(null, {
      style: function (feature) {
    		var color = '#f05a23';
    		var opacity = 0.7;
        return {stroke:  false, color: color, fillOpacity: opacity};
      },
			attribution: 'Satelligence 2018 Radar Powered Rapid Response Deforestation'
    })

  }
};

fetch('aoi.geojson')
.then(response => { return response.json() })
.then(data => L.geoJSON(data, {
  style: function (feature) {
    var color = 'black';
    var opacity = 0.3;
    return {'color':  color, 'weight': 2, 'fillOpacity': opacity, opacity: 0.4};
  },
  zindex: 10
}).addTo(map));

fetch('admitted/farms.geojson')
.then(response => { return response.json() })
.then(data => layers.foreground.admitted.addData(data));

fetch('parcs/parcs.geojson')
.then(response => { return response.json() })
.then(data => layers.foreground.parcs.addData(data));

fetch('parcels/illegals.geojson')
.then(response => { return response.json() })
.then(data => layers.foreground.illegals.addData(data))
.then(
  fetch('satelligence/2018.geojson')
  .then(response => { return response.json() })
  .then(data => layers.foreground.satelligence.addData(data))
);

const switchBackground = function (newActive) {
  for (layer in layers.background) {
    document.getElementById(layer).className = "";
    layers.background[layer].remove();
  }
  document.getElementById(newActive).className = "active";
  layers.background[newActive].addTo(map);
};

const toggleForeground = function (toggleLayer) {
  let layer = layers.foreground[toggleLayer];
  if (map.hasLayer(layer)) {
    document.getElementById(toggleLayer).className = "";
    layer.remove();
    if (toggleLayer === 'landcover') {
      legend.remove();
    }
  } else {
    document.getElementById(toggleLayer).className = "active";
    layer.addTo(map);
    if (toggleLayer === 'landcover') {
      legend.addTo(map);
    }
  }
}

switchBackground('satellite');
